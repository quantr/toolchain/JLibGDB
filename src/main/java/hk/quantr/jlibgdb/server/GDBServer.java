package hk.quantr.jlibgdb.server;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.ServerSocket;
import java.net.Socket;

/**
 *
 * @author Peter <peter@quantr.hk>
 */
public class GDBServer {

	public int port;
	private ServerSocket server;
	private GDBStub stub;

	public GDBServer(int port, GDBStub stub) {
		this.port = port;
		this.stub = stub;
	}

	public void start() throws IOException {
		System.out.println("start port " + port);
		server = new ServerSocket(port);

		while (true) {
			Socket socket = server.accept();
			System.out.println("incoming connection");
			handle(socket);
			socket.close();
		}
//		server.close();
	}

	public String getChecksum(String content) {
		int sum = 0;
		for (int x = 0; x < content.length(); x++) {
			sum += content.charAt(x);
		}
		return String.format("%02x", (sum % 256));
	}

	private void handle(Socket socket) throws IOException {
		InputStream is = socket.getInputStream();
		int b = is.read();
		if (b != '+') {
			System.out.println("not a '+'");
			return;
		}
		OutputStream os = socket.getOutputStream();
		os.write('+');
		int sum = 0;
		boolean start = false;
		String content = "";
		while ((b = is.read()) != -1) {
//			System.out.print((char) b);
			if (start && b != '#' && b != '$') {
				sum += b;
				content += (char) b;
//				System.out.println("> " + b);
			}
			if (b == '$') {
				start = true;
			} else if (b == '#') {
				char checksum1 = (char) is.read();
				char checksum2 = (char) is.read();
				int checksum = Integer.parseInt("" + checksum1 + checksum2, 16);
				if (sum % 256 != checksum) {
					System.out.println("\nchecksum error " + (sum % 256) + " != " + checksum);
					return;
				}
				os.write('+');
				
				String s = stub.process(content);
				os.write('$');
				os.write(s.getBytes());
				os.write('#');
				os.write(getChecksum(s).getBytes());

				// reset
				start = false;
				sum = 0;
				content = "";
				// end reset
//				System.out.println();

			}
		}

		System.out.print(b);
	}

	public void stop() {

	}
}
