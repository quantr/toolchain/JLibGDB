package hk.quantr.jlibgdb.client;

import hk.quantr.javalib.CommonLib;
import hk.quantr.javalib.swing.advancedswing.enhancedtextarea.EnhancedTextArea;
import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.InputStream;
import java.math.BigInteger;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Hashtable;
import java.util.concurrent.TimeUnit;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSpinner;
import javax.swing.JSplitPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.SpinnerNumberModel;
import javax.swing.border.EmptyBorder;

public class TestQemu extends JFrame {
	private JPanel contentPane;
	String keyCommand = "";
	JLibGDB libGDB = new JLibGDB("localhost", 25501);
	private JPanel panel;
	private JButton btnRunQemu;
	private Process p;
	private InputStream qemuInputStream;
	private JSplitPane splitPane;
	private EnhancedTextArea socketTextArea;
	private EnhancedTextArea logTextArea;
	private JButton btnCtrlC;
	private JButton btnC;
	private JButton btnGkdg;
	private JButton btnSi;
	private JButton btnSiGkdg;
	private JSpinner repeatSpinner;
	private JPanel statusPanel;
	private JLabel statusLabel;
	Date startDate;
	Date endDate;
	SimpleDateFormat format = new SimpleDateFormat("HH:mm:ss");
	private JCheckBox chckbxGuiOutput;
	private JPanel panel_1;
	private JPanel panel_2;
	private JTextField addressTextField;
	private JButton btnPhysicalMem;
	private JButton btnVirtualMem;
	private JPanel panel_3;
	private JLabel lblNewLabel;
	private JPanel panel_4;
	private JLabel lblNewLabel_1;
	private JButton btnPb;
	private JButton btnIsrunning;
	private JCheckBox stdoutCheckBox;
	private JTextField countTextField;

	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					TestQemu frame = new TestQemu();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	public TestQemu() {
		addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosing(WindowEvent e) {
				killQemu();
			}
		});

		if (System.getProperty("os.name").toLowerCase().toLowerCase().contains("mac")) {
//			com.apple.eawt.Application macApp = com.apple.eawt.Application.getApplication();
//			macApp.addApplicationListener(new ApplicationListener() {
//				public void handleQuit(ApplicationEvent arg0) {
//					killQemu();
//					System.exit(0);
//				}
//
//				public void handleAbout(ApplicationEvent arg0) {
//
//				}
//
//				public void handleOpenApplication(ApplicationEvent arg0) {
//
//				}
//
//				public void handleOpenFile(ApplicationEvent arg0) {
//
//				}
//
//				public void handlePreferences(ApplicationEvent arg0) {
//
//				}
//
//				public void handlePrintFile(ApplicationEvent arg0) {
//
//				}
//
//				public void handleReOpenApplication(ApplicationEvent arg0) {
//
//				}
//			});
		}
		setTitle("TestJLibGDB");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 846, 570);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new BorderLayout(0, 0));
		setContentPane(contentPane);

		splitPane = new JSplitPane();
		splitPane.setOrientation(JSplitPane.VERTICAL_SPLIT);
		contentPane.add(splitPane, BorderLayout.CENTER);
		splitPane.setDividerLocation(250);

		panel_3 = new JPanel();
		splitPane.setLeftComponent(panel_3);
		panel_3.setLayout(new BorderLayout(0, 0));

		lblNewLabel = new JLabel("Socket");
		panel_3.add(lblNewLabel, BorderLayout.NORTH);

		socketTextArea = new EnhancedTextArea();
		panel_3.add(socketTextArea, BorderLayout.CENTER);
		socketTextArea.addKeyListener(new KeyAdapter() {
			@Override
			public void keyTyped(KeyEvent e) {
				if (e.getKeyChar() == '\n') {
					execute(keyCommand);
					keyCommand = "";
				} else {
					keyCommand += e.getKeyChar();
				}
			}
		});

		panel_4 = new JPanel();
		splitPane.setRightComponent(panel_4);
		panel_4.setLayout(new BorderLayout(0, 0));

		lblNewLabel_1 = new JLabel("GDB stdout");
		panel_4.add(lblNewLabel_1, BorderLayout.NORTH);

		logTextArea = new EnhancedTextArea();
		panel_4.add(logTextArea, BorderLayout.CENTER);

		statusPanel = new JPanel();
		contentPane.add(statusPanel, BorderLayout.SOUTH);

		statusLabel = new JLabel("");
		statusPanel.add(statusLabel);

		panel_1 = new JPanel();
		contentPane.add(panel_1, BorderLayout.NORTH);
		panel_1.setLayout(new BoxLayout(panel_1, BoxLayout.Y_AXIS));

		panel = new JPanel();
		panel_1.add(panel);

		btnRunQemu = new JButton("Run qemu");
		btnRunQemu.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				runQemu();
			}
		});
		panel.add(btnRunQemu);

		btnCtrlC = new JButton("Ctrl - C");
		btnCtrlC.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				libGDB.ctrlC();
			}
		});

		chckbxGuiOutput = new JCheckBox("GUI");
		chckbxGuiOutput.setSelected(true);
		panel.add(chckbxGuiOutput);

		stdoutCheckBox = new JCheckBox("Stdout");
		panel.add(stdoutCheckBox);
		panel.add(btnCtrlC);

		btnC = new JButton("cont");
		btnC.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (chckbxGuiOutput.isSelected()) {
					socketTextArea.setText(socketTextArea.getText() + "c\n");
					execute("vCont;c");
				}
			}
		});
		panel.add(btnC);

		btnSi = new JButton("si");
		btnSi.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				new Thread() {
					public void run() {
						startDate = new Date();
						statusLabel.setText(format.format(startDate));
						Date tempDate = new Date();
						int speed = 0;
						for (int x = 0; x < (Integer) repeatSpinner.getValue(); x++) {
							if (chckbxGuiOutput.isSelected()) {
								socketTextArea.setText(socketTextArea.getText() + "si\n");
							} else {
								System.out.println("si");
							}
							execute("si");
							if (chckbxGuiOutput.isSelected()) {
								if (socketTextArea.getText().length() >= 500) {
									socketTextArea.setText("");
								}
							}
							endDate = new Date();
							if ((endDate.getTime() - tempDate.getTime()) > 1000) {
								statusLabel.setText(x + ", " + formatSeconds(endDate.getTime() - startDate.getTime()) + " , speed = " + speed);
								tempDate = endDate;
								speed = 0;
							} else {
								speed++;
							}
						}
						endDate = new Date();
						long milliSec = endDate.getTime() - startDate.getTime();
						long sec = milliSec / 1000;
						if (sec > 0) {
							statusLabel.setText(format.format(startDate) + " , " + format.format(endDate) + " = " + formatSeconds(milliSec) + " , average speed = "
									+ (Integer) repeatSpinner.getValue() / sec);
						}
					}
				}.start();
			}
		});

		btnGkdg = new JButton("gkd_g");
		btnGkdg.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				for (int x = 0; x < (Integer) repeatSpinner.getValue(); x++) {
					Hashtable<String, BigInteger> r = libGDB.register();
					if (chckbxGuiOutput.isSelected()) {
						socketTextArea.setText(socketTextArea.getText() + "gkd_g\n");
						socketTextArea.setText(socketTextArea.getText() + r + "\n");
					} else {
						System.out.println(r);
					}
				}
			}
		});
		panel.add(btnGkdg);

		repeatSpinner = new JSpinner(new SpinnerNumberModel(1, 1, 1000000000, 1));
		panel.add(repeatSpinner);
		panel.add(btnSi);

		btnSiGkdg = new JButton("si + gkd_g");
		btnSiGkdg.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				new Thread() {
					public void run() {
						startDate = new Date();
						statusLabel.setText(format.format(startDate));
						Date tempDate = new Date();
						int speed = 0;

						for (int x = 0; x < (Integer) repeatSpinner.getValue(); x++) {
							if (chckbxGuiOutput.isSelected()) {
								socketTextArea.setText(socketTextArea.getText() + "si\n");
							}
							execute("si");
							if (chckbxGuiOutput.isSelected()) {
								socketTextArea.setText(socketTextArea.getText() + "gkd_g\n");
							}
							execute("gkd_g");

							if (chckbxGuiOutput.isSelected() && socketTextArea.getText().length() >= 500) {
								socketTextArea.setText("");
							}
							endDate = new Date();
							if ((endDate.getTime() - tempDate.getTime()) > 1000) {
								statusLabel.setText(x + ", " + formatSeconds(endDate.getTime() - startDate.getTime()) + " , speed = " + speed);
								tempDate = endDate;
								speed = 0;
							} else {
								speed++;
							}
						}

						endDate = new Date();
						long milliSec = endDate.getTime() - startDate.getTime();
						long sec = milliSec / 1000;
						if (sec > 0) {
							statusLabel.setText(format.format(startDate) + " , " + format.format(endDate) + " = " + formatSeconds(milliSec) + " , average speed = "
									+ (Integer) repeatSpinner.getValue() / sec);
						}
					}
				}.start();
			}
		});
		panel.add(btnSiGkdg);

		panel_2 = new JPanel();
		panel_1.add(panel_2);

		addressTextField = new JTextField();
		addressTextField.setText("0xfffffff0");
		panel_2.add(addressTextField);
		addressTextField.setColumns(10);

		btnPhysicalMem = new JButton("physical mem");
		btnPhysicalMem.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				for (int x = 0; x < (Integer) repeatSpinner.getValue(); x++) {
					int bytes[] = libGDB.physicalMemory(CommonLib.string2BigInteger(addressTextField.getText()), CommonLib.string2int(countTextField.getText()));
					if (chckbxGuiOutput.isSelected()) {
						if (bytes == null) {
							socketTextArea.setText(socketTextArea.getText() + "error\n");
						} else {
							for (int b : bytes) {
								socketTextArea.setText(socketTextArea.getText() + Integer.toHexString(b) + " ");
							}
							socketTextArea.setText(socketTextArea.getText() + "\n");
						}
					}
				}
			}
		});

		countTextField = new JTextField();
		countTextField.setText("20");
		panel_2.add(countTextField);
		countTextField.setColumns(10);
		panel_2.add(btnPhysicalMem);

		btnVirtualMem = new JButton("virtual mem");
		btnVirtualMem.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				for (int x = 0; x < (Integer) repeatSpinner.getValue(); x++) {
					int bytes[] = libGDB.virtualMemory(CommonLib.string2BigInteger(addressTextField.getText()), CommonLib.string2int(countTextField.getText()));
					if (chckbxGuiOutput.isSelected()) {
						if (bytes == null) {
							socketTextArea.setText(socketTextArea.getText() + "error\n");
						} else {
							for (int b : bytes) {
								socketTextArea.setText(socketTextArea.getText() + Integer.toHexString(b) + " ");
							}
							socketTextArea.setText(socketTextArea.getText() + "\n");
						}
					}
				}
			}
		});
		panel_2.add(btnVirtualMem);

		btnPb = new JButton("pb");
		btnPb.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				libGDB.physicalBreakpoint(CommonLib.string2BigInteger(addressTextField.getText()));
			}
		});
		panel_2.add(btnPb);

		btnIsrunning = new JButton("isRunning?");
		btnIsrunning.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				execute("gkd_isrunning");
			}
		});
		panel_2.add(btnIsrunning);
		setLocationRelativeTo(null);
		runQemu();
	}

	protected void runQemu() {
		if (p != null) {
			p.destroy();
		}

		try {
			//String command = "/opt/local/bin/qemu-system-x86_64 -hda hd10meg.img -gdb tcp::1234 -k en-us -S";
			String command = "/Users/peter/qemu/bin/qemu-system-x86_64 -hda hd10meg.img -s -S -chardev socket,id=qmp,host=0.0.0.0,port=4444,server,nowait -mon chardev=qmp,mode=control";
			//String command = "/Users/peter/qemu/bin/qemu-system-x86_64 -hda hd10meg.img -s -S -chardev socket,id=qmp,host=0.0.0.0,port=4444,server,nowait -mon chardev=qmp,mode=control";
			p = Runtime.getRuntime().exec(command);
			qemuInputStream = p.getInputStream();
			new Thread() {
				public void run() {
					try {
						int x;
						while ((x = qemuInputStream.read()) != -1) {
							if (chckbxGuiOutput.isSelected()) {
								logTextArea.setText(logTextArea.getText() + (char) x);
								scrollToBotton(logTextArea.getTextArea());

								//								if (logTextArea.getText().length() > 2000) {
								//									logTextArea.setText("");
								//								}
							}
							if (stdoutCheckBox.isSelected()) {
								System.out.print((char) x);
							}
						}
					} catch (Exception ex) {
						ex.printStackTrace();
					}
				}
			}.start();
		} catch (Exception e1) {
			e1.printStackTrace();
		}
	}

	protected void execute(String command) {
		String returnStr = null;
		if (command.toLowerCase().equals("help")) {
			socketTextArea.setText(socketTextArea.getText() + "vcont?" + "\n");
			socketTextArea.setText(socketTextArea.getText() + "si" + "\n");
			socketTextArea.setText(socketTextArea.getText() + "g" + "\n");
		} else if (command.toLowerCase().equals("vcont?")) {
			returnStr = libGDB.sendSocketCommand("vCont");
		} else if (command.toLowerCase().equals("si")) {
			returnStr = libGDB.singleStep();
		} else if (command.toLowerCase().equals("gkd_isrunning")) {
			returnStr = String.valueOf(libGDB.isRunning());
		} else if (command.toLowerCase().equals("vcont;c")) {
			returnStr = libGDB._continue();
		} else if (command.toLowerCase().equals("r")) {
			returnStr = libGDB.sendSocketCommand("r");
		} else if (command.toLowerCase().equals("g")) {
			returnStr = libGDB.sendSocketCommand("g");
			char chars[] = returnStr.toCharArray();
			socketTextArea.setText(socketTextArea.getText() + returnStr + "\n");
			for (int x = 0; x < returnStr.length(); x += 8) {
				int xx = getInt(Integer.parseInt(String.valueOf(chars[x]) + chars[x + 1], 16), Integer.parseInt(String.valueOf(chars[x + 2]) + chars[x + 3], 16),
						Integer.parseInt(String.valueOf(chars[x + 4]) + chars[x + 5], 16), Integer.parseInt(String.valueOf(chars[x + 6]) + chars[x + 7], 16));
				socketTextArea.setText(socketTextArea.getText() + "\n" + x / 8 + "=" + Integer.toHexString(xx));
			}
			socketTextArea.setText(socketTextArea.getText() + "\n");

			Hashtable<String, Integer> registers = new Hashtable<String, Integer>();
			int x = 0;
			registers.put("eax", getInt(chars, x));
			x += 8;
			registers.put("ecx", getInt(chars, x));
			x += 8;
			registers.put("edx", getInt(chars, x));
			x += 8;
			registers.put("ebx", getInt(chars, x));
			x += 8;
			registers.put("esp", getInt(chars, x));
			x += 8;
			registers.put("ebp", getInt(chars, x));
			x += 8;
			registers.put("esi", getInt(chars, x));
			x += 8;
			registers.put("edi", getInt(chars, x));
			x += 8;
			registers.put("eip", getInt(chars, x));
			x += 8;
			registers.put("eflags", getInt(chars, x));
			x += 8;
			registers.put("cs", getInt(chars, x));
			x += 8;
			registers.put("ss", getInt(chars, x));
			x += 8;
			registers.put("ds", getInt(chars, x));
			x += 8;
			registers.put("es", getInt(chars, x));
			x += 8;
			registers.put("fs", getInt(chars, x));
			x += 8;
			registers.put("gs", getInt(chars, x));
			x += 8;
			registers.put("st0", getInt(chars, x));
			x += 16;
			registers.put("st1", getInt(chars, x));
			x += 16;
			registers.put("st2", getInt(chars, x));
			x += 16;
			registers.put("st3", getInt(chars, x));
			x += 16;
			registers.put("st4", getInt(chars, x));
			x += 16;
			registers.put("st5", getInt(chars, x));
			x += 16;
			registers.put("st6", getInt(chars, x));
			x += 16;
			registers.put("st7", getInt(chars, x));
			x += 16;
			registers.put("fctrl", getInt(chars, x));
			x += 8;
			registers.put("fstat", getInt(chars, x));
			x += 8;
			registers.put("ftag", getInt(chars, x));
			x += 8;
			registers.put("fiseg", getInt(chars, x));
			x += 8;
			registers.put("fioff", getInt(chars, x));
			x += 8;
			registers.put("foseg", getInt(chars, x));
			x += 8;
			registers.put("fooff", getInt(chars, x));
			x += 8;
			registers.put("fop", getInt(chars, x));
			x += 8;
			registers.put("xmm0", getInt(chars, x));
			x += 16;
			registers.put("xmm1", getInt(chars, x));
			x += 16;
			registers.put("xmm2", getInt(chars, x));
			x += 16;
			registers.put("xmm3", getInt(chars, x));
			x += 16;
			registers.put("xmm4", getInt(chars, x));
			x += 16;
			registers.put("xmm5", getInt(chars, x));
			x += 16;
			registers.put("xmm6", getInt(chars, x));
			x += 16;
			registers.put("xmm7", getInt(chars, x));
			x += 16;
			registers.put("mxcsr", getInt(chars, x));
			x += 16;
			//			System.out.println("x=" + x + "," + chars.length);
			//			System.out.println(registers.get("mxcsr"));
			//			System.out.println(registers.get("edx"));
		} else if (command.toLowerCase().equals("gkd_g")) {
			returnStr = libGDB.gkd_g();
		} else {
			socketTextArea.setText(socketTextArea.getText() + "unknown command :" + command + ", type help to see availble commands\n");
		}

		if (returnStr != null) {
			if (chckbxGuiOutput.isSelected()) {
				socketTextArea.setText(socketTextArea.getText() + "> " + returnStr + "\n");
				scrollToBotton(socketTextArea.getTextArea());
			}
			if (stdoutCheckBox.isSelected()) {
				System.out.println(returnStr);
			}
		}
	}

	private void scrollToBotton(JTextArea textArea) {
		int endPosition = textArea.getDocument().getLength();
		Rectangle bottom;
		try {
			bottom = textArea.modelToView(endPosition);
			textArea.scrollRectToVisible(bottom);
		} catch (Exception e) {
		}
	}

	public int getInt(char chars[], int offset) {
		return getInt(Integer.parseInt(String.valueOf(chars[offset]) + chars[offset + 1], 16), Integer.parseInt(String.valueOf(chars[offset + 2]) + chars[offset + 3], 16),
				Integer.parseInt(String.valueOf(chars[offset + 4]) + chars[offset + 5], 16), Integer.parseInt(String.valueOf(chars[offset + 6]) + chars[offset + 7], 16));
	}

	public int getInt(int b0, int b1, int b2, int b3) {
		int x = b0;
		x += b1 << 8;
		x += b2 << 16;
		x += b3 << 24;
		return x;
	}

	void killQemu() {
		if (p != null) {
			p.destroy();
		}
	}

	String formatSeconds(long milliSec) {
		return String.format("%d min, %d sec", TimeUnit.MILLISECONDS.toMinutes(milliSec),
				TimeUnit.MILLISECONDS.toSeconds(milliSec) - TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(milliSec)));
	}
}
