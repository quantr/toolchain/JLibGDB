package hk.quantr.jlibgdb.client;

import java.math.BigInteger;

public class Breakpoint {
	public BigInteger addr;
	public int flag;

	public Breakpoint(BigInteger addr, int flag) {
		super();
		this.addr = addr;
		this.flag = flag;
	}
}
