package hk.quantr.jlibgdb.client;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;

import net.sf.json.JSONObject;

public class QemuMonitor {

	public static void main(String[] args) {
		String command = "{ \"execute\": \"qmp_capabilities\" }";
		command += "{ \"execute\": \"query-status\" }";
		String json = sendCommand(command, "localhost", 4444);
		JSONObject j = JSONObject.fromObject(json).getJSONObject("return");
		System.out.println(j.get("running"));
	}

	public static synchronized String sendCommand(String command, String host, int port) {
		Socket socket = null;
		try {
			socket = new Socket(host, port);
			BufferedReader in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
			PrintWriter out = new PrintWriter(socket.getOutputStream());
			out.write(command);
			out.flush();
			in.readLine();
			in.readLine();
			return in.readLine();
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		} finally {
			if (socket != null) {
				try {
					socket.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
	}
}
