package hk.quantr.jlibgdb.client;

import hk.quantr.javalib.CommonLib;
import java.io.InputStream;
import java.io.OutputStream;
import java.math.BigInteger;
import java.net.Socket;
import java.util.Hashtable;
import java.util.Vector;
import org.apache.log4j.Logger;

import net.sf.json.JSONObject;

public class JLibGDB {

	String host;
	int port;
	Socket socket;
	InputStream in;
	OutputStream out;
	Vector<Breakpoint> breakpoints;
	Logger logger = Logger.getLogger("com.jlibgdb.JLibGDB");

	public static boolean debug = false;

	public JLibGDB(String host, int port) {
		logger.debug("JLibGDB " + host + ":" + port);
		this.host = host;
		this.port = port;
		breakpoints = new Vector<Breakpoint>();
	}

	private void connect() {
		try {
			logger.debug("connect " + host + ":" + port);
			socket = new Socket(host, port);
			socket.setKeepAlive(true);
			in = socket.getInputStream();
			out = socket.getOutputStream();
		} catch (Exception ex) {
			logger.error(ex.getMessage());
			System.exit(1);
		}
	}

	public synchronized String sendSocketCommand(String command) {
		if (debug) {
			logger.debug("    sendSocketCommand start " + command);
		}
		try {
			if (socket == null) {
				connect();
			}

			// send command
			out.write('+');
			String gdbCommand = "$" + command + "#" + checksum(command);
			out.write(gdbCommand.getBytes());
			out.flush();
			// end send command

			char c = (char) in.read();
			if (c != '+') {
				System.err.println("error, not a '+', it is a 0x" + Integer.toHexString(c));
				return null;
			}

			if (command.equals("vCont;c")) {
				return "";
			}

			char d = (char) in.read();
			if (d != '$') {
				System.err.println("error, not a '$', it is a 0x" + Integer.toHexString(d));
				return null;
			}
			String returnStr = "";
			while ((d = (char) in.read()) != '#') {
				returnStr += d;
			}
			String checksum = "";
			checksum += (char) in.read();
			checksum += (char) in.read();

			if (debug) {
				logger.debug("checksum=" + checksum);
				logger.debug("    sendSocketCommand end " + command);
			}
			return returnStr;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	public String si() {
		logger.debug("si");
		String command = "vCont;s:1;c";
		String returnStr = sendSocketCommand("$" + command + "#" + checksum(command));
		if (returnStr == null) {
			System.err.println("vCont error");
			return null;
		} else {
			return returnStr;
		}
	}

	public void ctrlC() {
		logger.debug("ctrlC");
		try {
			if (socket == null) {
				connect();
			}

			out.write(".".getBytes());
			out.flush();
			int c;
			while ((c = in.read()) != '#');
			in.read();
			in.read();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		logger.debug("ctrlC end");
	}

	public void sendRawData(String data) {
		logger.debug("sendRawData " + data);
		try {
			if (socket == null) {
				connect();
			}

			out.write(data.getBytes());
			out.flush();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}

	public String checksum(String str) {
		int checksum = 0;
		for (int x = 0; x < str.length(); x++) {
			checksum += str.getBytes()[x];
		}
		checksum %= 256;
		return String.format("%02x", checksum);
	}

	public String gkd_g() {
		logger.debug("gkd_g");
		return sendSocketCommand("gkd_g");
	}

	public boolean isRunning() {
		try {
			String command = "{ \"execute\": \"qmp_capabilities\" }";
			command += "{ \"execute\": \"query-status\" }";
			String json = QemuMonitor.sendCommand(command, "localhost", 4444);
			JSONObject j = JSONObject.fromObject(json).getJSONObject("return");
			return j.get("status").toString().equals("running");
		} catch (Exception ex) {
			ex.printStackTrace();
			return false;
		}
	}

	public String _continue() {
		logger.debug("_continue");
		return sendSocketCommand("vCont;c");
	}

	public String singleStep() {
		logger.debug("singleStep");
		return sendSocketCommand("vCont;s:1;c");
	}

	public int[] physicalMemory(BigInteger addr, int noOfByte) {
		logger.debug("physicalMemory 0x" + addr.toString(16) + " " + noOfByte);
		String r = sendSocketCommand("gkd_mp" + addr.toString(16) + "," + noOfByte);
		logger.debug("physicalMemory end 0x" + addr.toString(16) + " " + noOfByte);
		return toHexArray(r);
	}

	public int[] virtualMemory(BigInteger addr, int noOfByte) {
		logger.debug("virtualMemory 0x" + addr.toString(16) + " " + noOfByte);
		int bytes[] = new int[noOfByte];
		for (int x = 0; x < noOfByte; x++) {
			String r = sendSocketCommand("m" + addr.add(BigInteger.valueOf(x)).toString(16) + "," + 1);
			bytes[x] = Integer.parseInt(r, 16);
		}
		return bytes;
	}

	public boolean physicalBreakpoint(BigInteger addr) {
		logger.debug("physicalBreakpoint 0x" + addr.toString(16));
		breakpoints.add(new Breakpoint(addr, 0));
		String r = sendSocketCommand("Z0," + addr.toString(16) + ",1");
		return (r == null || !r.equals("OK")) ? false : true;
	}

	public boolean deletePhysicalBreakpoint(BigInteger addr) {
		logger.debug("deletePhysicalBreakpoint 0x" + addr.toString(16));
		for (Breakpoint breakpoint : breakpoints) {
			if (breakpoint.addr.equals(addr)) {
				breakpoints.remove(breakpoint);
			}
		}
		String r = sendSocketCommand("z0," + addr.toString(16) + ",1");
		return (r == null || !r.equals("OK")) ? false : true;
	}

	public boolean deleteAllPhysicalBreakpoint() {
		logger.debug("deleteAllPhysicalBreakpoint");
		breakpoints.clear();
		return true;
	}

	public Vector<Breakpoint> listBreakpoint() {
		logger.debug("listBreakpoint");
		return breakpoints;
	}

	public Hashtable<String, BigInteger> register() {
		logger.debug("register");
		Hashtable<String, BigInteger> ht = new Hashtable<String, BigInteger>();
		try {
			String r = gkd_g();
			if (r == null) {
				return null;
			}
			String temp[] = r.split(",");
			for (String s : temp) {
				String a[] = s.split("=");
				ht.put(a[0], CommonLib.string2BigInteger(a[1]));
			}
			return ht;
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return null;
	}

	public int[] toHexArray(String s) {
		if (s == null) {
			return null;
		} else if (s.length() % 2 != 0) {
			return null;
		} else {
			int bytes[] = new int[s.length() / 2];
			for (int x = 0; x < s.length(); x += 2) {
				bytes[x / 2] = Integer.parseInt(s.substring(x, x + 2), 16);
			}
			return bytes;
		}
	}
}
