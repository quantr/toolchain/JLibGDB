
import hk.quantr.jlibgdb.client.JLibGDB;
import java.text.SimpleDateFormat;
import java.util.Date;
import org.junit.Test;

/**
 *
 * @author Peter <peter@quantr.hk>
 */
public class TestConnectGDB {

	@Test
	public void test1() {
		SimpleDateFormat sdf = new SimpleDateFormat("hh:mm:ss");

		JLibGDB libGDB = new JLibGDB("localhost", 25501);
		String returnStr;
		returnStr = libGDB.sendSocketCommand("g");
		System.out.println(returnStr);

		for (long z = 0; z < 2000000l; z++) {
//		for (long z = 0; z < 6; z++) {
			if (z % 100000 == 0) {
				System.out.println(sdf.format(new Date()) + " : " + z);
			}

//			returnStr = libGDB.sendSocketCommand("g");
//			System.out.println(returnStr);
//
//			char chars[] = returnStr.toCharArray();
//			int x = 0;
//			while (x < returnStr.length()) {
//				long xx = getLong(Integer.parseInt(String.valueOf(chars[x]) + chars[x + 1], 16), Integer.parseInt(String.valueOf(chars[x + 2]) + chars[x + 3], 16),
//						Integer.parseInt(String.valueOf(chars[x + 4]) + chars[x + 5], 16), Integer.parseInt(String.valueOf(chars[x + 6]) + chars[x + 7], 16));
//				x += 8;
//
////				long xx2 = getLong(Integer.parseInt(String.valueOf(chars[x]) + chars[x + 1], 16), Integer.parseInt(String.valueOf(chars[x + 2]) + chars[x + 3], 16),
////						Integer.parseInt(String.valueOf(chars[x + 4]) + chars[x + 5], 16), Integer.parseInt(String.valueOf(chars[x + 6]) + chars[x + 7], 16));
////				x += 8;
////				System.out.printf("%x %x %d\n", xx, xx2, xx2);
//				System.out.printf("%x\n", xx);
//
//			}
//			System.out.println("-----------------------------");
			returnStr = libGDB.singleStep();
		}

	}

	public long getLong(int b0, int b1, int b2, int b3) {
		long x = b0;
		x += b1 << 8;
		x += b2 << 16;
		x += b3 << 24;
		return x;
	}
}
