
import hk.quantr.jlibgdb.server.GDBServer;
import hk.quantr.jlibgdb.server.GDBStub;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.junit.Test;

/*
 * Copyright 2023 Peter <peter@quantr.hk>.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
/**
 *
 * @author Peter <peter@quantr.hk>
 */
public class TestGDBServer {

	class Handler implements GDBStub {

		@Override
		public String process(String content) {
			System.out.println("> " + content);
			if (content.charAt(0) == 'q') {
				return "";
			}
			return "";
		}

	}

	@Test
	public void test() {
		try {
			Handler handler = new Handler();
			GDBServer server = new GDBServer(26000, handler);
			server.start();
		} catch (IOException ex) {
			Logger.getLogger(TestGDBServer.class.getName()).log(Level.SEVERE, null, ex);
		}
	}
}
